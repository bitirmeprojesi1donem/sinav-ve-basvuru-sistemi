/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example;

import com.example.connect.DbConnect;
import com.example.model.ExamModel;
import com.example.service.ExamService;
import org.junit.After;
import static org.junit.Assert.assertEquals;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 *
 * @author moriarty
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = DbConnect.class)
@ActiveProfiles(profiles = "test")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestForExam {
    
     @Autowired 
     ExamService exam;  
     ExamModel model = new ExamModel("Diferansiyel","butunleme", "Mustafa",121212L, "Matematik");
        
     
        @Test
        public void CreateExamTest()
        {
           
            assertEquals(true, exam.control(model));
            assertEquals(model.name, exam.CreateExam(model));
        }
        
        @Test
        public void DeleteExamTest()
        {
            assertEquals(false, exam.control(model));
            exam.DeleteExam(model);
            assertEquals(true, exam.control(model));
        }
        
        
        
        
        @Test
        public void UpdateExamTest()
        {
            
            assertEquals(false,model.equals(exam.UpdateExam(model)));
        }
        
        
}
