/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example;

import org.neo4j.ogm.session.SessionFactory;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.neo4j.config.Neo4jConfiguration;
import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 *
 * @author moriarty
 */

@EnableTransactionManagement
@EnableAutoConfiguration
@ComponentScan(basePackages = {"com.example.service"})
@Configuration
@EnableNeo4jRepositories(basePackages = "com.example.repository")
@Profile({"embedded", "test"})
public class TestDbConnect extends Neo4jConfiguration{

    @Bean
    public org.neo4j.ogm.config.Configuration getConfiguration() {
        org.neo4j.ogm.config.Configuration config = new org.neo4j.ogm.config.Configuration();
        config
                .driverConfiguration()
                .setDriverClassName("org.neo4j.ogm.drivers.embedded.driver.EmbeddedDriver");
        return config;
    }
    
    
    
    
    @Override
    public SessionFactory getSessionFactory() {
        return new SessionFactory(getConfiguration(), "com.example.model");
        
    }
    
}
