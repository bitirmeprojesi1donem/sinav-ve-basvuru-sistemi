/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.dec;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.declarative.Design;

/**
 *
 * @author moriarty
 */
@DesignRoot
public class declogin extends Panel{
    
    //protected Button yeniSinav;
    Table table = new Table();
    public declogin()
    {
        Design.read(this);
         setStyleName("addExamsPanel");
        
        VerticalLayout layout = new VerticalLayout();

        Grid grid = new Grid();
        grid.setStyleName("grid");
        grid.addColumn("Sınav Adı", String.class);
        grid.addColumn("Düzenle", String.class);
        grid.addColumn("Sil", String.class);

        grid.addRow("Nicolaus Copernicus", "1543", "Sil");
        grid.addRow("Galileo Galilei", "1564", "Sil");
        grid.addRow("Johannes Kepler", "1571", "Sil");
        
        layout.addComponent(new deneme());
        layout.addComponent(grid);
        setContent(layout);
    }
}
