/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.repository;

import com.example.model.ExamModel;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author moriarty
 */
@Repository
public interface ExamRepository extends GraphRepository<ExamModel>{
    
    @Query ("CREATE (e:Exam {name: {name} , type: {type} , teacher: {teacher}, date: {date}, departmentName: {departmentName}}) RETURN e.name")
    String Create(@Param("name") String name, @Param("type") String type, @Param("teacher") String teacher, @Param("date") Long date, @Param("departmentName") String departmentName);

    @Query ("MATCH (e:Exam {name: {name}, type: {type} , teacher: {teacher}, date: {date}, departmentName: {departmentName}}) RETURN count(e)")
    public int control(@Param("name") String name, @Param("type") String type, @Param("teacher") String teacher, @Param("date") Long date, @Param("departmentName") String departmentName);

    @Query ("MATCH (e:Exam {name: {name}, type: {type} , teacher: {teacher}, date: {date}, departmentName: {departmentName}}) DELETE e")
    public void deleteExam(@Param("name") String name, @Param("type") String type, @Param("teacher") String teacher, @Param("date") Long date, @Param("departmentName") String departmentName);
    
    
    
    
    
}
