/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.service;

import com.example.model.ExamModel;
import com.example.repository.ExamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author moriarty
 */
@Service
@Transactional
public class ExamService 
{
    @Autowired ExamRepository examRepository;
    
    public String CreateExam(ExamModel model)
    {
        
        return examRepository.Create(model.name, model.type, model.teacher, model.date, model.departmentName);
    
    }

    public boolean control(ExamModel model) {
        int kactane = examRepository.control(model.name, model.type, model.teacher, model.date, model.departmentName);
        if(kactane == 0 ) return true;
        return false;
    }

    public void DeleteExam(ExamModel model) {
        examRepository.deleteExam(model.name, model.type, model.teacher, model.date, model.departmentName);
    }

    
    
    public ExamModel UpdateExam(ExamModel model) 
    {
        return examRepository.save(model);
    }
    
}
